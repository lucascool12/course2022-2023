import rdflib
from rdflib.namespace import NamespaceManager, Namespace
import morph_kgc

g = morph_kgc.materialize("""
[landing]
mappings: rml_mapping.ttl
""")
nsm = NamespaceManager(g)
nsm.bind("ontology", "http://data.sfgov.org/ontology")
nsm.bind("landing", "http://example.org/landing/")
g.serialize("landing.ttl", "turtle")
