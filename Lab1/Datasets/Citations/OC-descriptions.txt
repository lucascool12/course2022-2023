OpenCitations descriptions:
[field "oci"] the Open Citation Identifier (OCI) for the citation;
[field "citing"] the PMID of the citing entity;
[field "cited"] the PMID of the cited entity;
[field "creation"] the creation date of the citation (i.e. the publication date of the citing entity);
[field "timespan"] the time span of the citation (i.e. the interval between the publication date of the cited entity and the publication date of the citing entity);
[field "journal_sc"] it records whether the citation is a journal self-citations (i.e. the citing and the cited entities are published in the same journal);
[field "author_sc"] it records whether the citation is an author self-citation (i.e. the citing and the cited entities have at least one author in common).

[field "oci"] the Open Citation Identifier (OCI) for the citation;
[field "snapshot"] the identifier of the snapshot;
[field "agent"] the name of the agent that have created the citation data;
[field "source"] the URL of the source dataset from where the citation data have been extracted;
[field "created"] the creation time of the citation data.
[field "invalidated"] the start of the destruction, cessation, or expiry of an existing entity by an activity;
[field "description"] a textual description of the activity made; 
[field "update"] the UPDATE SPARQL query that keeps track of which metadata have been modified.