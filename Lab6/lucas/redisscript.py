from __future__ import annotations
from typing import Dict, List, Tuple
import redis
from redisgraph import Node, Edge, Graph, Path
# from redis.commands.graph import Graph, Node, Edge, Path
import rdflib
from rdflib.namespace import Namespace, NamespaceManager
from rdflib.namespace._RDF import RDF
from rdflib.namespace._RDFS import RDFS
from rdflib.term import IdentifiedNode, URIRef, BNode, Literal

r = redis.Redis(host='localhost', port=6379)

redis_graph = Graph('social', r)
ex2 = Namespace("http://GP-onto.fi.upm.es/exercise2#")
rdf_graph = rdflib.Graph()
nsm = NamespaceManager(rdf_graph)
nsm.bind("ont", ex2)
nsm.bind("rdf", RDF)
nsm.bind("rdfs", RDFS)
rdf_graph.parse("graph.ttl")

s_to_node: Dict[IdentifiedNode, Node] = {}
node_to_node: List[Tuple[Node, str, Node]] = []

def get_node(s_to_node, s) -> Node:
    if s not in s_to_node:
        label: str = s.n3(nsm)
        label = label[label.find(":")+1:]
        s_to_node[s] = Node(label=label, properties={}) # type:ignore
    return s_to_node[s]

for s, p, o in rdf_graph:
    node = get_node(s_to_node, s)
    property: str = p.n3(nsm)
    property = property[property.find(":")+1:]
    if isinstance(o, Literal):
        node.properties[property] = str(o)
    else:
        node_obj = get_node(s_to_node, o)
        node_to_node.append((node, property, node_obj))

for node in s_to_node.values():
    redis_graph.add_node(node)

for n1, ed, n2 in node_to_node:
    edge = Edge(n1, ed, n2)
    redis_graph.add_edge(edge)

redis_graph.commit()

query = """MATCH (c)-[:type]->(:Class)
       RETURN c"""

result = redis_graph.query(query)

# Print resultset
result.pretty_print()


query = """MATCH (c)-[:subClassOf]->(:Establishment)
       RETURN c"""

result = redis_graph.query(query)

# Print resultset
result.pretty_print()


query = """MATCH (c)-[:type]->(:City)
       RETURN c"""

result = redis_graph.query(query)

query = """MATCH (e {label:"Santiago_de_Compostela"})
       RETURN e.hasInhabitantNumber"""

result = redis_graph.query(query)

# Print resultset
result.pretty_print()


query = """
    WITH ['Santiago_de_Compostela', 'Arzua'] as cities
    UNWIND cities AS city
    MATCH (e {label:city})
    RETURN e.label, e.hasInhabitantNumber"""

result = redis_graph.query(query)

result.pretty_print()


query = """MATCH (e)
    WHERE EXISTS(e.hasInhabitantNumber)
    RETURN labels(e) as label,e.hasInhabitantNumber
    ORDER BY label"""

result = redis_graph.query(query)

# Print resultset
result.pretty_print()


query = """MATCH (e)
    WHERE EXISTS(e.hasInhabitantNumber)
    RETURN labels(e) as label,e.hasInhabitantNumber
    ORDER BY label"""

result = redis_graph.query(query)

# Print resultset
result.pretty_print()


# All done, remove graph.
redis_graph.delete()
