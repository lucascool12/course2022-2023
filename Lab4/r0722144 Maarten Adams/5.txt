prefix ex2: <http://GP-onto.fi.upm.es/exercise2#>
select ?place ?inhabitants
{
 values ?place {
  ex2:Santiago_de_Compostela
  ex2:Arzua
 }
 ?place ex2:hasInhabitantNumber ?inhabitants
}
