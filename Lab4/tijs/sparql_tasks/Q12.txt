Describe the resource with rdfs:label "Madrid”

prefix gp: <http://GP-onto.fi.upm.es/exercise2#>
describe ?p where {
    ?p rdfs:label "Madrid".
}