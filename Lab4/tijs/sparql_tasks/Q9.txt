Get all postal address details of Pazo_Breogan

prefix gp: <http://GP-onto.fi.upm.es/exercise2#>
select ?s ?n where {
    gp:Pazo_Breogan gp:hasAddress ?a.
    ?a gp:hasStreet ?s;
       gp:hasNumber ?n.
}