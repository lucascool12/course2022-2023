Get all subclasses of class Location

prefix gp: <http://GP-onto.fi.upm.es/exercise2#>
select ?p where {
    ?p rdfs:subClassOf gp:Location.
}