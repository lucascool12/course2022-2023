prefix da: <http://GP-onto.fi.upm.es/exercise2#>

select ?local ?inhabitants where {
    ?local da:hasInhabitantNumber ?inhabitants.
    filter (xsd:integer(?inhabitants) > 200000)
}
order by ?local

