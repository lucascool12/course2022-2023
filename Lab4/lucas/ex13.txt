prefix da: <http://GP-onto.fi.upm.es/exercise2#>

construct {
    ?inst da:isIn ?loc
}
where {
    ?inst a/rdfs:subClassOf* da:TouristicLocation.
    ?inst da:isPlacedIn/da:inProvince ?loc.
}
